package stepdef;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import common.NewTourBase;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.RegisterPages;

public class Register extends NewTourBase {
	RegisterPages pf;
	@Given("^i am on the homepage$")
	public void i_am_on_the_homepage() {
		driver = getDriver();
		driver.get("http://newtours.demoaut.com/mercurywelcome.php");
		


	    
	}

	@When("^i click on the register button$")
	public void i_click_on_the_register_button() {
        driver.findElement(By.linkText("REGISTER")).click();
	    
	}

	@When("^i fill up all the personal informations$")
	public void i_fill_up_all_the_personal_informations() {
       driver.findElement(By.name("firstName")).sendKeys("Sabbir");
       driver.findElement(By.name("lastName")).sendKeys("Mamun");
       driver.findElement(By.id("userName")).sendKeys("guest@gmail.com");
       driver.findElement(By.id("email")).sendKeys("demo");
       driver.findElement(By.name("password")).sendKeys("1234");
       driver.findElement(By.name("confirmPassword")).sendKeys("1234");
       driver.findElement(By.name("register")).click();
	    
	}

	@Then("^registration should be successful$")
	public void registration_should_be_successful() {
		String title = driver.getTitle();
		Assert.assertEquals(title, "Register: Mercury Tours");
		driver.close();

	    
	}

}
