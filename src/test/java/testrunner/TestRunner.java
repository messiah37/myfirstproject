package testrunner;

import cucumber.api.CucumberOptions;

import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions (
		features = {"Feature"},
		glue = {"stepdef"},
		tags = {"@register"}
		
		)
public class TestRunner extends AbstractTestNGCucumberTests {

}
