package common;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class NewTourBase {
	public WebDriver driver;
	
	String browser = "chrome";
	
	public WebDriver getDriver() {
		if (browser.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\sabbi\\eclipse-workspace\\Newtour\\Driver\\chromedriver.exe");
			driver = new ChromeDriver();
			
		}
		else {
			System.out.println("Wrong Browser");
		}
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		return driver;
	}

}
